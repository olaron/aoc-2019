#!/usr/bin/env python3


from typing import TextIO, Tuple, List, Dict
import re

Chemical = Tuple[int, str]
Reaction = Tuple[List[Chemical], Chemical]
ReactionTable = Dict[str, Tuple[List[Chemical], int]]
Inventory = Dict[str, int]


def parse_reaction(line: str) -> Reaction:
    pattern = re.compile('([0-9]+) ([A-Z]+)')
    chemicals = pattern.findall(line)
    reaction = []
    for chemical in chemicals:
        reaction.append((int(chemical[0]), chemical[1]))
    return reaction[:-1], reaction[-1]


def parse_reactions(lines) -> ReactionTable:
    reactions = {}
    for line in lines:
        inputs, output = parse_reaction(line)
        reactions[output[1]] = (inputs, output[0])
    return reactions


def create_inventory(reactions: ReactionTable) -> Inventory:
    inventory = {}
    for chemical in reactions.keys():
        inventory[chemical] = 0
    inventory['ORE'] = 0
    return inventory


def get_ingredients_for_chemical(reactions: ReactionTable, chemical: Chemical) -> Tuple[List[Chemical], int]:
    ingredients, n_result = reactions[chemical[1]]
    factor = chemical[0]//n_result
    if chemical[0] % n_result != 0:
        factor += 1
    surplus = (n_result*factor) - chemical[0]
    result = []
    for ingredient in ingredients:
        result.append((ingredient[0] * factor, ingredient[1]))
    #print(result, '->', chemical, '+', surplus)
    return result, surplus


def consume_stock(inventory: Inventory, chemical: Chemical) -> Chemical:
    if inventory[chemical[1]] >= chemical[0]:
        inventory[chemical[1]] -= chemical[0]
        return 0, chemical[1]
    else:
        needed = (chemical[0] - inventory[chemical[1]], chemical[1])
        inventory[chemical[1]] = 0
        return needed


def get_ore_needed_to_get_chemical(reactions: ReactionTable, inventory: Inventory, chemical: Chemical) -> int:
    ore = 0
    ingredients, surplus = get_ingredients_for_chemical(reactions, chemical)
    inventory[chemical[1]] += surplus
    for ingredient in ingredients:
        needed_ingredient = consume_stock(inventory, ingredient)
        if needed_ingredient[0] == 0:
            continue
        if needed_ingredient[1] == 'ORE':
            ore += needed_ingredient[0]
        else:
            ore += get_ore_needed_to_get_chemical(reactions, inventory, needed_ingredient)
    return ore


def execute_14(input_file: TextIO) -> int:
    reactions = parse_reactions(input_file)
    inventory = create_inventory(reactions)
    ore = get_ore_needed_to_get_chemical(reactions, inventory, (1, 'FUEL'))
    print("Surplus:", inventory)
    return ore


def execute_14_2(input_file: TextIO) -> int:
    reactions = parse_reactions(input_file)
    ore_needed = get_ore_needed_to_get_chemical(reactions, create_inventory(reactions), (1, 'FUEL'))
    print("ORE needed for 1 FUEL:", ore_needed)
    inventory = create_inventory(reactions)
    inventory['ORE'] = 1000000000000
    ore_surplus = 0
    fuel_made = 0
    while ore_surplus == 0:
        print("Fuel made:", fuel_made, "; ORE remaining:", inventory['ORE'])
        fuel_to_make = inventory['ORE']//ore_needed
        if fuel_to_make == 0:
            fuel_to_make = 1
        print("Attempt to make", fuel_to_make, "FUEL")
        ore_surplus = get_ore_needed_to_get_chemical(reactions, inventory, (fuel_to_make, 'FUEL'))
        print("ORE surplus:", ore_surplus)
        fuel_made += fuel_to_make
    print("Surplus:", inventory)
    return fuel_made - 1


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('input_file_path', type=str,
                        help='the input file')

    args = parser.parse_args()
    with open(args.input_file_path) as file:
        ore = execute_14(file)
        print("Ore needed:", ore)

    with open(args.input_file_path) as file:
        fuel = execute_14_2(file)
        print("Fuel made with 1000000000000 ORE:", fuel)
